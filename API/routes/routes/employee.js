
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const employee= require('./model/employee');

router.get('/', (req,res,next) => {
    Employee.find()
    .then(result =>{
    res.status(200).json({
        employeeData:result
    });
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    });
});
router.get('/:id',(req,res,next) =>{
    console.log(req.params.id);
    Employee.findById(req.params.id)
    .then(result =>{
        res.status(200).json({
            employee:result
        })
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    })

})

router.post('/', (req,res,next) => {
/*console.log(req.body); */
    const employee =new Employee({
        _id:new mongoose.Types.ObjectId,
        name:req.body.name,
        email:req.body.email,
        phone:req.body.phone,
        city:req.body.city
    })
employee.save()
.then(result => {
    console.log(result);
    res.status(200).json({
        employeeData: result
    })
})
.catch(err => {
    console.log(err);
    res.status(500).json({
        error:err
    })     
 })
})

 
module.exports = router;