
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const product = require('./model/product');
const Product= require('./model/product');




/* get */
router.get('/', (req,res,next) => {
    Product.find()
    .exec()
.then(result => {
    res.status(200).json({
        Product: result
    })
})
})
/* get by ID*/ 
router.get('/', (req,res,next) => {
    const_id = req.params.id;
    Product.find()
    .then(result =>{
    res.status(200).json({
        productData:result
    });
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    });
});
router.get('/:id',(req,res,next) =>{
    console.log(req.params.id);
    Product.findById(_id)
    .then(result =>{
        res.status(200).json({
            product:result
        })
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    })

})

router.post('/', (req,res,next) => {
console.log(req.body); 
    const product =new Product({
        _id:new mongoose.Types.ObjectId,
        code:req.body.code,
        title:req.body.title,
        discription:req.body.discription,
        mrp:req.body.mrp,
        sp:req.body.sp,
        discouuntpercent:req.body.discouuntpercent,
        imagePath:req.body.imagePath

    })
product.save()
.then(result => {
    console.log(result);
    res.status(200).json({
        productData: result
    })
})
.catch(err => {
    console.log(err);
    res.status(500).json({
        error:err
    })     
 })
})

/*  delete request */
router.delete('/:id',(req, res,next)=>{
    Product.remove({_id:req.params.id})
    .then(result=>{
        res.status(200).json({
            message:'product deleted',
            result:result
        })
    })
    .catch(err=>{
        res.status(500).json({
            error:err
        })
    })
    })
    /*put request */ 
    router.put('/:id',(req, res,next)=>{
        console.log(req.params.id)
     Product.findOneAndUpdate({_id:req.params.id},{
         $set:{
                code:req.body.code,
                title:req.body.title,
                discription:req.body.discription,
                mrp:req.body.mrp,
                sp:req.body.sp,
                discouuntpercent:req.body.discouuntpercent,
                imagePath:req.body.imagePath
         }
        })
    
    .then(result=>{
        res.status(200).json({
            updated_product: result
        })
    })
    .catch(err=>{
        console.log(err);
        res.status(500).json({
            errilla:err
        })
    })
       })
 
module.exports = router;