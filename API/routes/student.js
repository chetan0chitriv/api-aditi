
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Student= require('./model/student');


/*---------------------------------------------------------------------*/
const  multer = require('multer');
const upload =multer({des:'upload/'});

const storage = multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,'./upload/');
    },
    filename:function(req,file,cb){
        cb(null,new Date().toISOString()+file.originalname);
    }

});


/*-----------------------*/














router.get('/', (req,res,next) => {
    Student.find()
    .then(result =>{
    res.status(200).json({
        StudentData:result
    });
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    });
});
router.get('/:id',(req,res,next) =>{
    console.log(req.params.id);
    Student.findById(req.params.id)
    .then(result =>{
        res.status(200).json({
            student:result
        })
    })
    .catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    })

})

router.post('/',upload.single('studentimg'), (req,res,next) => {
/*console.log(req.body); */
console.log(req.file);
    const student =new Student({
        _id:new mongoose.Types.ObjectId,
        name:req.body.name,
        email:req.body.email,
        mobile:req.body.mobile,
        city:req.body.city
    })
student.save()
.then(result => {
    console.log(result);
    res.status(200).json({
        StudentData: result
    })
})
.catch(err => {
    console.log(err);
    res.status(500).json({
        error:err
    })     
 })
})

 /*DELETE REQ*/
 router.delete('/:id',(req, res,next)=>{
    Student.remove({_id:req.params.id})
    .then(result=>{
        res.status(200).json({
            message:'student deleted',
            result:result
        })
    })
    .catch(err=>{
        res.status(500).json({
            error:err
        })
    })
    })

    /*put req */
    router.put('/:id',(req, res,next)=>{
        console.log(req.params.id)
     Student.findOneAndUpdate({_id:req.params.id},{
         $set:{
            _id:new mongoose.Types.ObjectId,
            name:req.body.name,
            email:req.body.email,
            mobile:req.body.mobile,
            city:req.body.city
            
         }
        })
    
    .then(result=>{
        res.status(200).json({
            updated_student: result
        })
    })
    .catch(err=>{
        console.log(err);
        res.status(500).json({
            error:err
        })
    })
       })
module.exports = router;